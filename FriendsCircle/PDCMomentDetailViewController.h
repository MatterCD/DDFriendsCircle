//
//  PDCMomentDetailViewController.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/4.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  PDCMoment;

@interface PDCMomentDetailViewController : UIViewController

@property (nonatomic, strong) PDCMoment *moment;

@end
