//
//  NSString+PDC.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/16.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "NSString+PDC.h"

@implementation NSString (PDC)

- (CGSize)sizeWithFont:(UIFont *)font {
    CGSize size = [self sizeWithFont:font constrainedToSize:CGSizeMake(MAXFLOAT, font.lineHeight)];
    return size;
}

- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)csize {
    CGSize size = CGSizeZero;
    if (!font) {
        return size;
    }
    NSDictionary *attributes = @{NSFontAttributeName : font};
    // NSString class method: boundingRectWithSize:options:attributes:context is available only on ios7.0 sdk.
    CGRect rect = [self boundingRectWithSize:csize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    size = rect.size;
    
    NSInteger labWidth,labHeight;
    labWidth = (NSInteger)size.width;
    labHeight = (NSInteger)size.height;
    
    if (size.width > labWidth) {
        labWidth = (NSInteger)size.width + 1;
    }
    if (size.height > labHeight) {
        labHeight = (NSInteger)size.height + 1;
    }
    
    CGSize lableNewSize = CGSizeMake(labWidth, labHeight);
    
    return lableNewSize;
}

- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)csize lineBreakMode:(NSLineBreakMode)lineBreakMode {
    CGSize size = CGSizeZero;
    if (!font) {
        return size;
    }
    if ([self respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineBreakMode:lineBreakMode];
        NSDictionary *attributes = @{NSFontAttributeName: font,NSParagraphStyleAttributeName:style};
        // NSString class method: boundingRectWithSize:options:attributes:context is available only on ios7.0 sdk.
        CGRect rect = [self boundingRectWithSize:csize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        size = rect.size;
    }
    NSInteger labWidth,labHeight;
    labWidth = (NSInteger)size.width;
    labHeight = (NSInteger)size.height;
    
    if (size.width > labWidth) {
        labWidth = (NSInteger)size.width + 1;
    }
    if (size.height > labHeight) {
        labHeight = (NSInteger)size.height + 1;
    }
    
    CGSize lableNewSize = CGSizeMake(labWidth, labHeight);
    
    return lableNewSize;
}

- (CGSize)sizeWithMaxWidth:(CGFloat)maxWidth font:(UIFont *)font numberOfLines:(NSUInteger)numberOfLines {
    return [self sizeWithFont:font constrainedToSize:CGSizeMake(maxWidth,numberOfLines * font.lineHeight)];
}

- (NSDate *)dateOfFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter dateFromString:self];
}

@end
