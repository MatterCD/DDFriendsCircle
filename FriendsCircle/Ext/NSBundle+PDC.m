//
//  NSBundle+PDC.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/9.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "NSBundle+PDC.h"
#import "PDCMomentsViewController.h"
#import "PDCLocalizeControl.h"

@implementation NSBundle (PDC)

+ (NSString *)pdcLocalizedStringForKey:(NSString *)key {
    return [self pdcLocalizedStringForKey:key value:nil];
}

+ (NSString *)pdcLocalizedStringForKey:(NSString *)key value:(NSString *)value {
    NSString *language = [NSLocale preferredLanguages].firstObject;
    NSString *userLanguage = [PDCLocalizeControl userLanguage];
    if (userLanguage) {
        language = userLanguage;
    }
    if ([language hasPrefix:@"en"]) {
        language = @"en";
    } else if ([language hasPrefix:@"zh"]) {
        if ([language rangeOfString:@"Hans"].location != NSNotFound) {
            language = @"zh-Hans";
        } else {
            language = @"zh-Hans";
        }
    } else {
        language = @"en";
    }
    NSBundle *frameworkBundle = [NSBundle bundleForClass:PDCMomentsViewController.class];
    NSURL *friendsCircleBundleURL = [frameworkBundle URLForResource:@"FriendsCircle" withExtension:@"bundle"];
    NSBundle *friendsCircleBundle = [NSBundle bundleWithURL:friendsCircleBundleURL];
    NSBundle *localizeResBundle = [NSBundle bundleWithPath:[friendsCircleBundle pathForResource:language ofType:@"lproj"]];
    
    value = [localizeResBundle localizedStringForKey:key value:value table:nil];
    return [[NSBundle mainBundle] localizedStringForKey:key value:value table:nil];
}

@end
