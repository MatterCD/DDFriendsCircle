//
//  PDCChatInputBox.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/15.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PDCChatInputBoxDelegate <NSObject>

- (void)chatInputBoxBeginEditing;
- (void)chatInputBoxEndEditing;
- (void)chatInputBoxSendText:(NSString *)text;

@optional

- (BOOL)chatInputBoxShouldBeginEditing;

@end

@interface PDCChatInputBox : UIView

@property (nonatomic, weak) id<PDCChatInputBoxDelegate> delegate;

@property (nonatomic, copy) NSString *currentText;

@property (nonatomic, assign,readonly) BOOL isFirstUp;

@property CGFloat previousTextViewHeight;

- (void)clearText;

- (void)setTextViewPlaceHolder:(NSString *)placeholder;

- (void)keyboardUp;

- (void)keyboardDown;

@end
