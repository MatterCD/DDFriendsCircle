//
//  PDCMomentView.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/10/13.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDCMoment;
@class PDCMomentView;
@class PDCSudokuBoxView;

@protocol PDCMomentViewDelegate <NSObject>

@optional
- (void)momentView:(PDCMomentView *)momentView doDeleteMoment:(PDCMoment *)moment;
- (void)momentView:(PDCMomentView *)momentView doLikeMoment:(PDCMoment *)moment;
- (void)momentView:(PDCMomentView *)momentView doCommentMoment:(PDCMoment *)moment;
- (void)momentView:(PDCMomentView *)momentView doTapUserAvatarOfMoment:(PDCMoment *)moment;
- (void)momentView:(PDCMomentView *)momentView doTapLikeUser:(NSString *)userId ofMoment:(PDCMoment *)moment;

@required
- (void)momentView:(PDCMomentView *)momentView doTapBoxView:(PDCSudokuBoxView *)boxView withMoment:(PDCMoment *)moment andImageIndex:(NSInteger)imageIndex;

@end

@interface PDCMomentView : UITableViewHeaderFooterView

@property (nonatomic, weak) id<PDCMomentViewDelegate> delegate;

+ (CGFloat)heightForModel:(PDCMoment *)moment;

- (void)bindModel:(PDCMoment *)moment withIndex:(NSInteger)index;

@end
